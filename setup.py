import setuptools
from distutils.core import Command

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="django-rasterfile",  # Replace with your own username
    version="0.0.1",
    author="Axel Wegener",
    author_email="python@sparse-space.de",
    description="A subclass of the django filefield containing convenience methods for dealing with raster files",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/awmath/django-rasterfile",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3',
    install_requires=['django', 'rasterio', 'mercantile'],
)
