from django.db import models
from django.db.models.fields.files import FieldFile
from django.contrib.gis.gdal import GDALRaster, GDALException
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError

import rasterio
from rasterio.windows import Window, transform
from rasterio.io import MemoryFile

import mercantile


class RasterFieldFile(FieldFile):
    """
    Custom FieldFile subclass which contains convenience methods for working with raster files.
    """
    def tms_tile(self, x, y, z):
        """
        Returns a crop of the raster within a specific TMS tile defined by it's x,y and zoom parameters.

        Args:
            FieldFile ([type]): [description]
            x (float): TMS x coordinate
            y (float): TMS y coordinate
            z (float): TMS zoom parameter

        Returns:
            tuple: the profile of the original raster file as well as the numpy array containing the data
        """
        bounds = mercantile.bounds(x, y, z)
        with rasterio.open(self.path) as src:
            upper_left = Point(x=bounds.west, y=bounds.north, srid=4326)
            lower_right = Point(x=bounds.east, y=bounds.south, srid=4326)

            if (src.crs.to_epsg() != 4326):
                upper_left.transform(src.crs.to_epsg())
                lower_right.transform(src.crs.to_epsg())

            bounds_window = src.window(upper_left.x, lower_right.y, lower_right.x, upper_left.y)

            bounds_window = bounds_window.intersection(Window(0, 0, src.width, src.height))

            # Get the window with integer height
            # and width that contains the bounds window.
            # TODO: is this really necessary here?
            # out_window = bounds_window.round_lengths(op='ceil')
            out_window = Window(
                round(bounds_window.col_off), round(bounds_window.row_off), round(bounds_window.width), round(bounds_window.height))

            height = int(out_window.height)
            width = int(out_window.width)

            new_meta = src.meta.copy()
            new_meta.update({'height': height, 'width': width, 'transform': transform(out_window, src.transform)})
            return new_meta, src.read(window=out_window, out_shape=(src.count, height, width))


class RasterFileField(models.FileField):
    """
    This is a custom model Field for django providing convenience methode for dealing with raster files.
    Compared to the existing RasterField this field is based on an actual file in the filesystem and works by accessing this file directly.
    """
    attr_class = RasterFieldFile

    def pre_save(self, model_instance, add):
        """Extends the parent pre_save method with a check if the given file is a raster file or not.

        raises: ValidationError if file is not a valid GDAL raster file.
        """
        file = super().pre_save(model_instance, add)
        try:
            GDALRaster(file.path)
        except GDALException as e:
            raise ValidationError('{0} is not a valid GDAL raster file'.format(file.path))

        return file
