import os

from django.test import TestCase
from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.gis.gdal import GDALRaster
from django_rasterfile.fields import RasterFileField
from tests.models import TestModel

from tempfile import NamedTemporaryFile
import rasterio
from rasterio.windows import Window
import numpy as np


class TestRasterField(TestCase):
    RASTER_NAME = 'testraster.tiff'

    def setUp(self):
        self.raster = GDALRaster(
            {
                'driver': 'GTiff',
                'name': self.RASTER_NAME,
                'width': 100,
                'height': 10,
                'srid': '25833',
                'origin': [0, 0],
                'scale': [10, 10],
                'bands': [{
                    "data": range(1000),
                    "nodata_value": 0
                }]
            })

    def testRasterValidation(self):
        test_file = NamedTemporaryFile(dir='')

        # valid raster
        try:
            TestModel.objects.create(raster=self.RASTER_NAME)
        except Exception as e:
            self.fail('Object creation failed {0}'.format(str(e)))

        # invalid raster
        with self.assertRaises(ValidationError):
            TestModel.objects.create(raster=test_file.name)

    def testTileMapService(self):
        # check with a checkered raster file of size 32x16 from -180,90 to 180,-90
        # at zoom 4 there should be only one pixel
        checker_object = TestModel.objects.create(raster='tests/noise3857.tif')
        _, data = checker_object.raster.tms_tile(3, 3, 3)

        self.assertTrue((data == np.array([[[227, 100], [251, 199]], [[122, 78], [227, 145]], [[187, 197], [214, 44]]])).all())

    def tearDown(self):
        os.remove(self.RASTER_NAME)
        return super().tearDown()
