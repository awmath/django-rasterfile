from django.db import models
from django_rasterfile.fields import RasterFileField


class TestModel(models.Model):
    raster = RasterFileField()