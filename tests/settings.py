DATABASES = {'default': {'ENGINE': 'django.db.backends.sqlite3'}}

SECRET_KEY = 'notsosecret'

INSTALLED_APPS = ['django_rasterfile', 'tests']