[![pipeline status](https://gitlab.com/awmath/django-rasterfile/badges/master/pipeline.svg)](https://gitlab.com/awmath/django-rasterfile/commits/master)
[![coverage report](https://gitlab.com/awmath/django-rasterfile/badges/master/coverage.svg)](https://gitlab.com/awmath/django-rasterfile/commits/master)

# Description

A subclass of the django filefield containing convenience methods for dealing with raster files

# Installation

`pip install git+https://gitlab.com/awmath/django-rasterfile`

# Usage

from django-rasterfile import RasterFileField
from django.db import models

```python
class TestModel(models.Model):
    raster = RasterFileField()
```

For further options refer to the [django documentation](https://docs.djangoproject.com/en/dev/ref/models/fields/#django.db.models.FileField) for FileField.
